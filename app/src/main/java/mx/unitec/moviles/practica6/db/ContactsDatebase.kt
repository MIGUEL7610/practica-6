package mx.unitec.moviles.practica6.db

import android.content.Context
import android.support.v4.app.INotificationSideChannel
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import mx.unitec.moviles.practica6.dao.ContactDao
import mx.unitec.moviles.practica6.model.Contact

@Database (entities = [Contact::class],version = 1)
abstract class ContactsDatebase: RoomDatabase() {
    abstract fun contactDao():ContactDao
    companion object{
        private const val DATABASE_NAME = "contacts_database"
        @Volatile
        private var INSTANCE: ContactsDatebase?= null

        fun getInstance( context: Context): ContactsDatebase?{
            INSTANCE?: synchronized(this){
                INSTANCE= Room.databaseBuilder(
                    context.applicationContext,
                    ContactsDatebase::class.java,
                    DATABASE_NAME
                ).build()
            }
            return INSTANCE
        }

    }
}