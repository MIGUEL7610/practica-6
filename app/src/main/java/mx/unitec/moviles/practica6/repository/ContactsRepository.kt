package mx.unitec.moviles.practica6.repository

import android.app.Application
import androidx.lifecycle.LiveData
import mx.unitec.moviles.practica6.dao.ContactDao
import mx.unitec.moviles.practica6.db.ContactsDatebase
import mx.unitec.moviles.practica6.model.Contact

class ContactsRepository(application: Application) {
    private val contactDao: ContactDao?=ContactsDatebase.getInstance(application)?.contactDao()

    suspend fun insert(contact: Contact){//inicia el metodo insert
        contactDao!!.insert(contact)

    }//termina el metodo insert

    fun getContacts():LiveData<List<Contact>>{
        return contactDao!!.getOrderedAgenda()
    }
}